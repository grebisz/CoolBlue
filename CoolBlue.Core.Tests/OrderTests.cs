﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using CoolBlue.EF;
using CoolBlue.Core.Interfaces.RepositoryFunctions;
using CoolBlue.Core.Concrete;
using System.Collections.Generic;

namespace CoolBlue.Core.Tests
{
    [TestClass]
    public class OrderTests
    {
        private static IOrders core_order = null;

        [ClassInitialize]
        public static void Init(TestContext tstcontext)
        {
            Repository repo = new Repository(Enums.RepositoryFactoryTypes.Mock);
            core_order = repo.Orders;
            core_order.AddOrder(new Order()
            {
                Id = 0,
                Customer = new Customer() { Firstname = "Dummy", Lastname = "User", Email = "test@email.com" },
                Bundles = new System.Collections.Generic.List<ProductBundle>(),                
                Products = new System.Collections.Generic.List<Product>(),
                Invoicetotal = 0,
            });
            core_order.AddOrder(new Order()
            {
                Id = 1,
                Customer = new Customer() { Firstname = "Dummy2", Lastname = "User2", Email = "test2@email.com" },
                Bundles = new System.Collections.Generic.List<ProductBundle>(),
                Products = new System.Collections.Generic.List<Product>(),
                Invoicetotal = 0,
            });
        }

        [TestMethod]
        [ExpectedException(typeof(System.InvalidOperationException))]
        [TestCategory("Order Tests - Negative Tests")]
        public void AddProductToOrder_NoOrder()
        {
            core_order.AddProductToOrder(99, new Product() { Title = "Lenovo IdeaPad 510-15IKB 80SV00K5MH", Description = "Lenovo Laptop", Price = 699 });
        }

        [TestMethod]
        [ExpectedException(typeof(System.InvalidOperationException))]
        [TestCategory("Order Tests - Negative Tests")]
        public void AddMultipleProductsToOrder_NoOrder()
        {
            List<Product> products = new List<Product>();
            products.Add(new Product() { Title = "Lenovo IdeaPad 510-15IKB 80SV00K5MH", Description = "Lenovo Laptop", Price = 699 });
            products.Add(new Product() { Title = "HP 15-ba071nd", Description = "Hewlett Packard Laptop", Price = 369 });
            products.Add(new Product() { Title = "MSI GP72M 7REX-687NL Leopard Pro", Description = "MSI Leopard Pro", Price = 1549 });
            products.Add(new Product() { Title = "LG 43UH610V", Description = "LG 4K UHD TV", Price = 412 });
            core_order.AddMultipleProductsToOrder(99, products);
        }

        [TestMethod]
        [TestCategory("Order Tests - Positive Tests")]
        public void AddProductToOrder()
        {
            core_order.AddProductToOrder(0, new Product() { Id = 1000, Title = "New Product 101" });
            List<Order> ordersearch = core_order.Search(0);
            Assert.AreEqual(1, ordersearch.Count);
            Assert.AreEqual(1, ordersearch[0].Products.Count);
            Assert.AreEqual(1000, ordersearch[0].Products[0].Id);
        }

        [TestMethod]
        public void AddMultipleProductsToOrder()
        {
            List<Product> products = new List<Product>();
            products.Add(new Product() { Title = "Lenovo IdeaPad 510-15IKB 80SV00K5MH", Description = "Lenovo Laptop", Price = 699 });
            products.Add(new Product() { Title = "HP 15-ba071nd", Description = "Hewlett Packard Laptop", Price = 369 });
            products.Add(new Product() { Title = "MSI GP72M 7REX-687NL Leopard Pro", Description = "MSI Leopard Pro", Price = 1549 });
            products.Add(new Product() { Title = "LG 43UH610V", Description = "LG 4K UHD TV", Price = 412 });
            core_order.AddMultipleProductsToOrder(1, products);
            List<Order> ordersearch = core_order.Search(1);
            Assert.AreEqual(1, ordersearch.Count);            
            Assert.AreEqual(4, ordersearch[0].Products.Count);
        }
    }
}
