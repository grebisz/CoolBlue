﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using CoolBlue.Core.Interfaces;
using CoolBlue.Core.Concrete;
using CoolBlue.Core.Interfaces.RepositoryFunctions;

namespace CoolBlue.Core.Tests
{
    [TestClass]
    public class AuthenticationTests
    {
        private static IAuthentication core_auth = null;
        
        [ClassInitialize]
        public static void Init(TestContext tstcontext)
        {
            Repository repo = new Repository(Enums.RepositoryFactoryTypes.Mock);                    
            core_auth = repo.Authentication;
            core_auth.AddEmployee(new EF.Employee()
            {
                Active = true,
                Firstname = "Greg",
                Lastname = "Rebisz",
                Username = "grebisz",
                Password = "panopticon"
            });
            core_auth.AddEmployee(new EF.Employee()
            {
                Active = false,
                Firstname = "helios",
                Lastname = "somelastname",
                Password = "heliospass",
                Username = "helios"
            });
        }

        [TestMethod]
        [TestCategory("Authentication Tests - Negative Tests")]
        public void TestInvalidLogin_password()
        {
            Assert.IsFalse(core_auth.Authenticate("grebisz", "panop"), "Test yielded true when it should have been false (invalid credentials)");
        }

        [TestMethod]
        [TestCategory("Authentication Tests - Negative Tests")]
        public void TestInvalidLogin_username()
        {
            Assert.IsFalse(core_auth.Authenticate("nouser", "panopticon"), "Test yielded true when it should have been false (invalid credentials)");
        }

        [TestMethod]
        [TestCategory("Authentication Tests - Negative Tests")]
        public void TestInvalidLogin_disabled()
        {
            Assert.IsFalse(core_auth.Authenticate("helios", "heliospass"), "Test yielded true when it should have been false (inactive employee");
        }

        [TestMethod]
        [TestCategory("Authentication Tests - Positive Tests")]
        public void TestValidLogin_grebisz_panopticon()
        {
            Assert.IsTrue(core_auth.Authenticate("grebisz", "panopticon"), "Test yielded false when it should have been true (valid credentials)");
        }
    }
}
