﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using CoolBlue.EF;
using System.Collections.Generic;
using CoolBlue.Core.Interfaces.RepositoryFunctions;
using CoolBlue.Core.Concrete;

namespace CoolBlue.Core.Tests
{
    [TestClass]
    public class ProductBundleTests
    {
        private static IProductBundles core_bundles = null;
        private static IProducts core_products = null;

        [ClassInitialize]
        public static void Init(TestContext tstcontext)
        {
            Repository repo = new Repository(Enums.RepositoryFactoryTypes.Mock);
            core_bundles = repo.ProductBundles;
            core_products = repo.Products;
        }

        [TestMethod]
        [TestCategory("Product Bundles - Negative Tests")]
        public void GetRelatedBundlesForProduct_NonExistant()
        {
            Assert.AreEqual(0, core_bundles.GetRelatedBundles("HP 15-ba071nd").Count);
        }

        [TestMethod]
        [TestCategory("Product Bundles - Positive Tests")]
        public void GetRelatedBundlesForProduct()
        {
            Assert.AreEqual(2, core_bundles.GetRelatedBundles("VOGEL'S THIN 205").Count);
        }
    }
}
