﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using CoolBlue.EF;
using CoolBlue.Core.Interfaces.RepositoryFunctions;
using CoolBlue.Core.Concrete;

namespace CoolBlue.Core.Tests
{
    [TestClass]
    public class ProductsTests
    {
        private static IProducts core_prod = null;

        [ClassInitialize]
        public static void Init(TestContext tstcontext)
        {
            Repository repo = new Repository(Enums.RepositoryFactoryTypes.Mock);
            core_prod = repo.Products;
        }

        [TestMethod]
        [TestCategory("Product Tests - Negative Tests")]
        public void SearchForProduct_Invalid()
        {
            List<Product> product = core_prod.Search("Huawei P8 Lite");
            Assert.AreEqual(0, product.Count);
        }

        [TestMethod]
        [TestCategory("Product Tests - Positive Tests")]
        public void GetAll()
        {
            List<Product> product = core_prod.GetAll();
            Assert.AreEqual(7, product.Count);
        }

        [TestMethod]
        [TestCategory("Product Tests - Positive Tests")]
        public void SearchForProduct_Valid()
        {
            List<Product> product = core_prod.Search("VOGEL'S");
            Assert.AreEqual(2, product.Count);
        }
        
    }
}
