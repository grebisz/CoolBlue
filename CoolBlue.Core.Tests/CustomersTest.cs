﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using CoolBlue.Core.Interfaces.RepositoryFunctions;
using CoolBlue.Core.Concrete;

namespace CoolBlue.Core.Tests
{
    [TestClass]
    public class CustomersTest
    {
        private static ICustomers core_customers = null;

        [ClassInitialize]
        public static void Init(TestContext tstcontext)
        {
            Repository repo = new Repository(Enums.RepositoryFactoryTypes.Mock);
            core_customers = repo.Customer;
            core_customers.AddCustomer(new EF.Customer()
            {
                Firstname = "Joe",
                Lastname = "Soap",
                Email = "JoeSoap@gmail.com"
            });
        }

        [TestMethod]
        [TestCategory("Customers Tests - Negative Tests")]
        public void GetCustomer_Invalid()
        {
            Assert.AreEqual(0, core_customers.Search("Jane").Count);
        }

        [TestMethod]
        [TestCategory("Customers Tests - Positive Tests")]
        public void GetCustomer_Valid()
        {
            Assert.AreEqual(1, core_customers.Search("Joe").Count);
        }

        [TestMethod]
        [TestCategory("Customers Tests - Positive Tests")]
        public void GetCustomerBySurname_Valid()
        {
            Assert.AreEqual(1, core_customers.Search("Soap").Count);
        }

        [TestMethod]
        [TestCategory("Customers Tests - Positive Tests")]
        public void GetCustomerByEmail_Valid()
        {
            Assert.AreEqual(1, core_customers.Search("JoeSoap@gmail.com").Count);
        }
    }
}
