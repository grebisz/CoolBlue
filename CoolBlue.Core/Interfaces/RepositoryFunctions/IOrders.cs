﻿using CoolBlue.EF;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CoolBlue.Core.Interfaces.RepositoryFunctions
{
    public interface IOrders
    {
        List<Order> Search(int search);
        List<Order> Search(string search);
        void AddOrder(Order o);
        void AddProductToOrder(int orderid, Product p);
        void AddProductBundleToOrder(int orderid, ProductBundle p);
        void AddMultipleProductsToOrder(int orderid, List<Product> products);
        void AddMultipleBundlesToOrder(int orderid, List<ProductBundle> productbundles);
    }
}
