﻿using CoolBlue.EF;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CoolBlue.Core.Interfaces.RepositoryFunctions
{
    public interface IProducts
    {
        List<Product> Search(string search);
        List<Product> GetAll();
        Product GetById(int id);
        void AddProduct(Product p);
    }
}
