﻿using CoolBlue.EF;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CoolBlue.Core.Interfaces.RepositoryFunctions
{
    public interface IProductBundles
    {
        List<ProductBundle> GetRelatedBundles(string title);
        void AddBundle(ProductBundle bundle);
        List<ProductBundle> GetAll();
        ProductBundle GetById(int id);
    }
}
