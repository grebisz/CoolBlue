﻿using CoolBlue.EF;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CoolBlue.Core.Interfaces.RepositoryFunctions
{
    public interface IAuthentication
    {
        bool Authenticate(string username, string password);
        void AddEmployee(Employee e);
    }
}
