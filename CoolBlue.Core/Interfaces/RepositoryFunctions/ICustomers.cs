﻿using CoolBlue.EF;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CoolBlue.Core.Interfaces.RepositoryFunctions
{
    public interface ICustomers
    {
        List<Customer> Search(string search);
        void AddCustomer(Customer c);
    }
}
