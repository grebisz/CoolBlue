﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CoolBlue.Core.Enums
{
    public enum RepositoryFactoryTypes
    {
        Mock,
        EntityFramework
    }
}
