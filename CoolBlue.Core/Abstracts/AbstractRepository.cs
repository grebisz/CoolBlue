﻿using CoolBlue.Core.Interfaces;
using CoolBlue.Core.Interfaces.RepositoryFunctions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CoolBlue.Core.Abstracts
{
    public abstract class AbstractRepository
    {
        public IAuthentication Authentication { get; set; }
        public ICustomers Customer { get; set; }
        public IOrders Orders { get; set; }
        public IProducts Products { get; set; }
        public IProductBundles ProductBundles { get; set; }
    }
}
