﻿using CoolBlue.Core.Interfaces;
using CoolBlue.Core.Interfaces.RepositoryFunctions;
using CoolBlue.EF;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CoolBlue.Core.MockRepository
{
    public class Authentication : IAuthentication
    {
        readonly List<Employee> _context = null;
        public Authentication(List<Employee> context)
        {
            _context = context;
        }

        public void AddEmployee(Employee e)
        {
            _context.Add(e);
        }

        /// <summary>
        /// Authenticates a user by username or password
        /// </summary>
        /// <param name="username">The username in plain text</param>
        /// <param name="password">Tha password in plain text until encryption is required</param>
        /// <returns></returns>
        public bool Authenticate(string username, string password)
        {
            Employee emp = _context.Where(
                p => p.Username == username && 
                p.Password == password && p.Active).FirstOrDefault();
            return (emp != null);
        }
    }
}
