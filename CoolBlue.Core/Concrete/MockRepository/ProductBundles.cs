﻿using CoolBlue.Core.Interfaces;
using CoolBlue.Core.Interfaces.RepositoryFunctions;
using CoolBlue.EF;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CoolBlue.Core.MockRepository
{
    public class ProductBundles : IProductBundles
    {
        readonly List<ProductBundle> _context = null;
        public ProductBundles(List<ProductBundle> context)
        {
            _context = context;
        }

        public void AddBundle(ProductBundle bundle)
        {
            _context.Add(bundle);
        }

        public List<ProductBundle> GetAll()
        {
            return _context;
        }

        public ProductBundle GetById(int id)
        {
            return _context.Where(p => p.Id == id).ToList()[0];
        }

        /// <summary>
        /// A method that returns bundles that relate to a product
        /// </summary>
        /// <param name="prod">The product to be searched</param>
        /// <returns>A list of product bundles that match</returns>
        public List<ProductBundle> GetRelatedBundles(string title)
        {
            //There is probably a more elegant way to do this
            //Although this was the first implementation to come
            //to mind
            List<ProductBundle> returnlist = new List<ProductBundle>();
            foreach(ProductBundle bundle in _context.ToList())
            {
                if (bundle.Products.Where(p => p.Title == title).Count() > 0)
                {
                    returnlist.Add(bundle);
                    continue;
                }
            }
            return returnlist;
        }
    }
}
