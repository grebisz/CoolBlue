﻿using CoolBlue.Core.Interfaces;
using CoolBlue.Core.Interfaces.RepositoryFunctions;
using CoolBlue.EF;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CoolBlue.Core.MockRepository
{
    public class Orders : IOrders
    {
        readonly List<Order> _context = null;
        public Orders(List<Order> context)
        {
            _context = context;
        }

        public void AddMultipleBundlesToOrder(int orderid, List<ProductBundle> productbundles)
        {
            foreach(ProductBundle bundle in productbundles)
            {
                AddProductBundleToOrder(orderid, bundle);
            }
        }

        public void AddMultipleProductsToOrder(int orderid, List<Product> products)
        {
            foreach(Product prod in products)
            {
                AddProductToOrder(orderid, prod);
            }
        }

        public void AddOrder(Order o)
        {
            _context.Add(o);
        }

        public void AddProductBundleToOrder(int orderid, ProductBundle p)
        {
            List<Order> orders = Search(orderid);
            if (orders.Count == 1)
            {
                orders[0].Bundles.Add(p);
                return;
            }
            else if (orders.Count > 0)
            {
                throw new Exception(string.Format("Multiple orders returned for id {0}", orderid));
            }
            throw new InvalidOperationException(string.Format("Order not found for id {0}", orderid));
        }

        public void AddProductToOrder(int orderid, Product p)
        {
            List<Order> orders = Search(orderid);
            if(orders.Count == 1)
            {
                orders[0].Products.Add(p);
                return;
            }
            else if(orders.Count > 0)
            {
                throw new Exception(string.Format("Multiple orders returned for id {0}", orderid));
            }
            throw new InvalidOperationException(string.Format("Order not found for id {0}", orderid));
        }

        /// <summary>
        /// Searches for an order by its ID
        /// </summary>
        /// <param name="search">the order identifier</param>
        /// <returns>a List of orders that match (should be only one)</returns>
        public List<Order> Search(int search)
        {
            return _context.Where(p => p.Id == search).ToList();
        }
        /// <summary>
        /// Searches for an order by the customer details
        /// </summary>
        /// <param name="search">The string to be searched</param>
        /// <returns>A list of orders that match</returns>
        public List<Order> Search(string search)
        {
            return _context.Where(
                p =>
                p.Customer.Firstname.ToLower().StartsWith(search.ToLower()) ||
                p.Customer.Lastname.ToLower().Contains(search.ToLower()) ||
                p.Customer.Email.ToLower() == search.ToLower()).ToList();
        }
    }
}
