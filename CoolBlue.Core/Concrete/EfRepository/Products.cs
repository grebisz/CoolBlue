﻿using CoolBlue.Core.Interfaces;
using CoolBlue.Core.Interfaces.RepositoryFunctions;
using CoolBlue.EF;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CoolBlue.Core.EfRepository
{
    public class Products : IProducts
    {
        readonly Context _context = null;
        public Products(Context context)
        {
            _context = context;
        }
        /// <summary>
        /// Searches for a product where the title starts with a search string
        /// or the description contains any reference to the search string
        /// </summary>
        /// <param name="search">the search string</param>
        /// <returns>a result list of products</returns>
        public List<Product> Search(string search)
        {
            return _context.Products.Where(p => p.Title.ToLower().StartsWith(search.ToLower()) || p.Description.ToLower().Contains(search.ToLower())).ToList();
        }

        public List<Product> GetAll()
        {
            return _context.Products.ToList();
        }

        public void AddProduct(Product p)
        {
            throw new NotImplementedException();
        }

        public Product GetById(int id)
        {
            List<Product> products = _context.Products.Where(p => p.Id == id).ToList();
            if (products.Count == 1) return products[0];
            return null;            
        }
    }
}
