﻿using CoolBlue.Core.Interfaces;
using CoolBlue.Core.Interfaces.RepositoryFunctions;
using CoolBlue.EF;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CoolBlue.Core.EfRepository
{
    public class Authentication : IAuthentication
    {
        readonly Context _context = null;
        public Authentication(Context context)
        {
            _context = context;
        }

        public void AddEmployee(Employee e)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Authenticates a user by username or password
        /// </summary>
        /// <param name="username">The username in plain text</param>
        /// <param name="password">Tha password in plain text until encryption is required</param>
        /// <returns></returns>
        public bool Authenticate(string username, string password)
        {
            Employee emp = _context.Employees.Where(p => p.Username == username && p.Password == password && p.Active).FirstOrDefault();
            return (emp != null);
        }
    }
}
