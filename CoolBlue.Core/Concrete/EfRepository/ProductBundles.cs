﻿using CoolBlue.Core.Interfaces;
using CoolBlue.Core.Interfaces.RepositoryFunctions;
using CoolBlue.EF;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CoolBlue.Core.EfRepository
{
    public class ProductBundles : IProductBundles
    {
        readonly Context _context = null;
        public ProductBundles(Context context)
        {
            _context = context;
        }

        public void AddBundle(ProductBundle bundle)
        {
            throw new NotImplementedException();
        }

        public List<ProductBundle> GetAll()
        {
            throw new NotImplementedException();
        }

        public ProductBundle GetById(int id)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// A method that returns bundles that relate to a product
        /// </summary>
        /// <param name="prod">The product to be searched</param>
        /// <returns>A list of product bundles that match</returns>
        public List<ProductBundle> GetRelatedBundles(string title)
        {
            //There is probably a more elegant way to do this
            //Although this was the first implementation to come
            //to mind
            List<ProductBundle> returnlist = new List<ProductBundle>();
            foreach(ProductBundle bundle in _context.Productbundles.Include("products").ToList())
            {
                if (bundle.Products.Where(p => p.Title == title).Count() > 0)
                {
                    returnlist.Add(bundle);
                    continue;
                }
            }
            return returnlist;
        }
    }
}
