﻿using CoolBlue.Core.Interfaces;
using CoolBlue.Core.Interfaces.RepositoryFunctions;
using CoolBlue.EF;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CoolBlue.Core.EfRepository
{
    public class Customers : ICustomers
    {
        readonly Context _context = null;
        public Customers(Context context)
        {
            _context = context;
        }

        public void AddCustomer(Customer c)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Fetches a customer by a string matching against the first name
        /// last name
        /// or email address
        /// </summary>
        /// <param name="search">the string to be searched</param>
        /// <returns>A list of customers that match</returns>
        public List<Customer> Search(string search)
        {
            return _context.Customers.Where(p => 
                p.Firstname.ToLower().StartsWith(search.ToLower()) || 
                p.Lastname.ToLower().Contains(search.ToLower()) ||
                p.Email.ToLower() == search.ToLower()).ToList();
        }
    }
}
