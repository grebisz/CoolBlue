﻿using CoolBlue.Core.Interfaces;
using CoolBlue.Core.Interfaces.RepositoryFunctions;
using CoolBlue.EF;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CoolBlue.Core.EfRepository
{
    public class Orders : IOrders
    {
        readonly Context _context = null;
        public Orders(Context context)
        {
            _context = context;
        }

        public void AddMultipleBundlesToOrder(int orderid, List<ProductBundle> productbundles)
        {
            throw new NotImplementedException();
        }

        public void AddMultipleProductsToOrder(int orderid, List<Product> products)
        {
            throw new NotImplementedException();
        }

        public void AddOrder(Order o)
        {
            throw new NotImplementedException();
        }

        public void AddProductBundleToOrder(int orderid, ProductBundle p)
        {
            throw new NotImplementedException();
        }

        public void AddProductToOrder(int orderid, Product p)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Searches for an order by its ID
        /// </summary>
        /// <param name="search">the order identifier</param>
        /// <returns>a List of orders that match (should be only one)</returns>
        public List<Order> Search(int search)
        {
            return _context.Orders.Where(p => p.Id == search).ToList();
        }
        /// <summary>
        /// Searches for an order by the customer details
        /// </summary>
        /// <param name="search">The string to be searched</param>
        /// <returns>A list of orders that match</returns>
        public List<Order> Search(string search)
        {
            return _context.Orders.Include("Customer").Where(
                p =>
                p.Customer.Firstname.ToLower().StartsWith(search.ToLower()) ||
                p.Customer.Lastname.ToLower().Contains(search.ToLower()) ||
                p.Customer.Email.ToLower() == search.ToLower()).ToList();
        }
    }
}
