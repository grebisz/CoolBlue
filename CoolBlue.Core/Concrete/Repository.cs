﻿using CoolBlue.Core.Abstracts;
using CoolBlue.Core.Enums;
using CoolBlue.Core.Interfaces.RepositoryFunctions;
using CoolBlue.EF;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CoolBlue.Core.Concrete
{
    public class Repository : AbstractRepository
    {

        public Repository(RepositoryFactoryTypes type)
        {
            //Enum will force the value to be passed since it is not nullabe
            //Hence no default arguement specified below.
            switch (type)
            {
                case RepositoryFactoryTypes.EntityFramework:
                    Context context = new Context();
                    this.Authentication = new EfRepository.Authentication(context);
                    this.Customer = new EfRepository.Customers(context);
                    this.Orders = new EfRepository.Orders(context);
                    this.ProductBundles = new EfRepository.ProductBundles(context);
                    this.Products = new EfRepository.Products(context);                    
                    break;
                case RepositoryFactoryTypes.Mock:
                    this.Authentication = new MockRepository.Authentication(new List<Employee>());
                    this.Customer = new MockRepository.Customers(new List<Customer>());
                    this.Orders = new MockRepository.Orders(new List<Order>());
                    this.ProductBundles = new MockRepository.ProductBundles(new List<ProductBundle>());
                    this.Products = new MockRepository.Products(new List<Product>());
                    seed();
                    break;
                
            }
        }

        private void seed()
        {            
            this.Products.AddProduct(new Product() { Title = "Lenovo IdeaPad 510-15IKB 80SV00K5MH", Description = "Lenovo Laptop", Price = 699 });
            this.Products.AddProduct(new Product() { Title = "HP 15-ba071nd", Description = "Hewlett Packard Laptop", Price = 369 });
            this.Products.AddProduct(new Product() { Title = "MSI GP72M 7REX-687NL Leopard Pro", Description = "MSI Leopard Pro", Price = 1549 });
            this.Products.AddProduct(new Product() { Title = "LG 43UH610V", Description = "LG 4K UHD TV", Price = 412 });
            this.Products.AddProduct(new Product() { Title = "Samsung UE43KU6000", Description = "Samsung 4k UHD TV", Price = 604 });
            this.Products.AddProduct(new Product() { Title = "VOGEL'S THIN 205", Description = "Display Mounting Rack", Price = 59 });
            this.Products.AddProduct(new Product() { Title = "VOGEL'S 305", Description = "Display Heavy-Duty Mounting Rack", Price = 89 });

            Product vogel = this.Products.Search("VOGEL'S THIN 205")[0];
            Product Samsung = this.Products.Search("Samsung UE43KU6000")[0];
            Product LG = this.Products.Search("LG 43UH610V")[0];

            ProductBundle bundle1 = new ProductBundle()
            {
                Title = "Samsung UHD TV + Mount",
                Description = "Mounted UHD TV setup",
                Price = vogel.Price + Samsung.Price,
                Products = new System.Collections.Generic.List<Product>()
            };
            bundle1.Products.Add(vogel);
            bundle1.Products.Add(Samsung);

            ProductBundle bundle2 = new ProductBundle()
            {
                Title = "LG UHD TV + Mount",
                Description = "Mounted UHD TV setup",
                Price = vogel.Price + LG.Price,
                Products = new System.Collections.Generic.List<Product>()
            };
            bundle2.Products.Add(vogel);
            bundle2.Products.Add(LG);

            this.ProductBundles.AddBundle(bundle1);
            this.ProductBundles.AddBundle(bundle2);
        }
    }
}
