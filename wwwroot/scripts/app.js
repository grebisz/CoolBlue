angular.module('grebisz', [])
  .controller('demoController', ['$scope', function($scope) {
    var Reaction = this;
    var apiurl = "http://localhost:7506/";

    self.menushow = false;
    self.showMenu = function(){
      if(self.menushow){
        self.menushow = false;
        $('#navbar').css("display", "none");
      }
      else{
        self.menushow = true;
        $('#navbar').css("display", "block");
      }
    }

    self.Tabs = ['home', 'news', 'events', 'shop', 'podcast', 'artists', 'team'];
    self.Tab = function(name){
      for(var i=0;i<self.Tabs.length;i++)
      {
        $('#' + self.Tabs[i]).css("display", 'none');
        $('#' + self.Tabs[i] + "li").removeClass('active');
      }
      $('#' + name).css('display', 'block');
      $('#' + name + 'li').addClass('active');
    }
    
    self.Home = null;
    $.ajax({
      url: apiurl + "Home/012"
    }).done(function(data,result) {
      self.Home = data;
      $scope.$apply();
    });
  }]);
