﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CoolBlue.EF
{
    public class Context : DbContext
    {
        public Context() : base("CoolBlue") {

        }
        public DbSet<Employee> Employees { get; set; }
        public DbSet<Customer> Customers { get; set; }
        public DbSet<Product> Products { get; set; }
        public DbSet<ProductBundle> Productbundles { get; set; }
        public DbSet<Order> Orders { get; set; }
    }
}
