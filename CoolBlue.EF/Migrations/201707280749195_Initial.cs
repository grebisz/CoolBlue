namespace CoolBlue.EF.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Initial : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Customers",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        firstname = c.String(),
                        lastname = c.String(),
                        email = c.String(),
                    })
                .PrimaryKey(t => t.id);
            
            CreateTable(
                "dbo.Employees",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        firstname = c.String(),
                        lastname = c.String(),
                        active = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.id);
            
            CreateTable(
                "dbo.Orders",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        invoicetotal = c.Double(nullable: false),
                        customer_id = c.Int(),
                    })
                .PrimaryKey(t => t.id)
                .ForeignKey("dbo.Customers", t => t.customer_id)
                .Index(t => t.customer_id);
            
            CreateTable(
                "dbo.ProductBundles",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        title = c.String(),
                        description = c.String(),
                        Order_id = c.Int(),
                    })
                .PrimaryKey(t => t.id)
                .ForeignKey("dbo.Orders", t => t.Order_id)
                .Index(t => t.Order_id);
            
            CreateTable(
                "dbo.Products",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        title = c.String(),
                        description = c.String(),
                        price = c.Double(nullable: false),
                        ProductBundle_id = c.Int(),
                        Order_id = c.Int(),
                    })
                .PrimaryKey(t => t.id)
                .ForeignKey("dbo.ProductBundles", t => t.ProductBundle_id)
                .ForeignKey("dbo.Orders", t => t.Order_id)
                .Index(t => t.ProductBundle_id)
                .Index(t => t.Order_id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Products", "Order_id", "dbo.Orders");
            DropForeignKey("dbo.Orders", "customer_id", "dbo.Customers");
            DropForeignKey("dbo.ProductBundles", "Order_id", "dbo.Orders");
            DropForeignKey("dbo.Products", "ProductBundle_id", "dbo.ProductBundles");
            DropIndex("dbo.Products", new[] { "Order_id" });
            DropIndex("dbo.Products", new[] { "ProductBundle_id" });
            DropIndex("dbo.ProductBundles", new[] { "Order_id" });
            DropIndex("dbo.Orders", new[] { "customer_id" });
            DropTable("dbo.Products");
            DropTable("dbo.ProductBundles");
            DropTable("dbo.Orders");
            DropTable("dbo.Employees");
            DropTable("dbo.Customers");
        }
    }
}
