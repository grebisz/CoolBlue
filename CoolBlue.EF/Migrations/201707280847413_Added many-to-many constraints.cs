namespace CoolBlue.EF.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Addedmanytomanyconstraints : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Products", "ProductBundle_id", "dbo.ProductBundles");
            DropForeignKey("dbo.ProductBundles", "Order_id", "dbo.Orders");
            DropForeignKey("dbo.Products", "Order_id", "dbo.Orders");
            DropIndex("dbo.ProductBundles", new[] { "Order_id" });
            DropIndex("dbo.Products", new[] { "ProductBundle_id" });
            DropIndex("dbo.Products", new[] { "Order_id" });
            CreateTable(
                "dbo.ProductBundleOrders",
                c => new
                    {
                        ProductBundle_id = c.Int(nullable: false),
                        Order_id = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.ProductBundle_id, t.Order_id })
                .ForeignKey("dbo.ProductBundles", t => t.ProductBundle_id, cascadeDelete: true)
                .ForeignKey("dbo.Orders", t => t.Order_id, cascadeDelete: true)
                .Index(t => t.ProductBundle_id)
                .Index(t => t.Order_id);
            
            CreateTable(
                "dbo.ProductProductBundles",
                c => new
                    {
                        Product_id = c.Int(nullable: false),
                        ProductBundle_id = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.Product_id, t.ProductBundle_id })
                .ForeignKey("dbo.Products", t => t.Product_id, cascadeDelete: true)
                .ForeignKey("dbo.ProductBundles", t => t.ProductBundle_id, cascadeDelete: true)
                .Index(t => t.Product_id)
                .Index(t => t.ProductBundle_id);
            
            CreateTable(
                "dbo.ProductOrders",
                c => new
                    {
                        Product_id = c.Int(nullable: false),
                        Order_id = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.Product_id, t.Order_id })
                .ForeignKey("dbo.Products", t => t.Product_id, cascadeDelete: true)
                .ForeignKey("dbo.Orders", t => t.Order_id, cascadeDelete: true)
                .Index(t => t.Product_id)
                .Index(t => t.Order_id);
            
            DropColumn("dbo.ProductBundles", "Order_id");
            DropColumn("dbo.Products", "ProductBundle_id");
            DropColumn("dbo.Products", "Order_id");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Products", "Order_id", c => c.Int());
            AddColumn("dbo.Products", "ProductBundle_id", c => c.Int());
            AddColumn("dbo.ProductBundles", "Order_id", c => c.Int());
            DropForeignKey("dbo.ProductOrders", "Order_id", "dbo.Orders");
            DropForeignKey("dbo.ProductOrders", "Product_id", "dbo.Products");
            DropForeignKey("dbo.ProductProductBundles", "ProductBundle_id", "dbo.ProductBundles");
            DropForeignKey("dbo.ProductProductBundles", "Product_id", "dbo.Products");
            DropForeignKey("dbo.ProductBundleOrders", "Order_id", "dbo.Orders");
            DropForeignKey("dbo.ProductBundleOrders", "ProductBundle_id", "dbo.ProductBundles");
            DropIndex("dbo.ProductOrders", new[] { "Order_id" });
            DropIndex("dbo.ProductOrders", new[] { "Product_id" });
            DropIndex("dbo.ProductProductBundles", new[] { "ProductBundle_id" });
            DropIndex("dbo.ProductProductBundles", new[] { "Product_id" });
            DropIndex("dbo.ProductBundleOrders", new[] { "Order_id" });
            DropIndex("dbo.ProductBundleOrders", new[] { "ProductBundle_id" });
            DropTable("dbo.ProductOrders");
            DropTable("dbo.ProductProductBundles");
            DropTable("dbo.ProductBundleOrders");
            CreateIndex("dbo.Products", "Order_id");
            CreateIndex("dbo.Products", "ProductBundle_id");
            CreateIndex("dbo.ProductBundles", "Order_id");
            AddForeignKey("dbo.Products", "Order_id", "dbo.Orders", "id");
            AddForeignKey("dbo.ProductBundles", "Order_id", "dbo.Orders", "id");
            AddForeignKey("dbo.Products", "ProductBundle_id", "dbo.ProductBundles", "id");
        }
    }
}
