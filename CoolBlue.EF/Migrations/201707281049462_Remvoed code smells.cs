namespace CoolBlue.EF.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Remvoedcodesmells : DbMigration
    {
        public override void Up()
        {
            DropIndex("dbo.Orders", new[] { "customer_id" });
            DropIndex("dbo.ProductBundleOrders", new[] { "ProductBundle_id" });
            DropIndex("dbo.ProductBundleOrders", new[] { "Order_id" });
            DropIndex("dbo.ProductProductBundles", new[] { "Product_id" });
            DropIndex("dbo.ProductProductBundles", new[] { "ProductBundle_id" });
            DropIndex("dbo.ProductOrders", new[] { "Product_id" });
            DropIndex("dbo.ProductOrders", new[] { "Order_id" });
            CreateIndex("dbo.Orders", "Customer_Id");
            CreateIndex("dbo.ProductBundleOrders", "ProductBundle_Id");
            CreateIndex("dbo.ProductBundleOrders", "Order_Id");
            CreateIndex("dbo.ProductProductBundles", "Product_Id");
            CreateIndex("dbo.ProductProductBundles", "ProductBundle_Id");
            CreateIndex("dbo.ProductOrders", "Product_Id");
            CreateIndex("dbo.ProductOrders", "Order_Id");
        }
        
        public override void Down()
        {
            DropIndex("dbo.ProductOrders", new[] { "Order_Id" });
            DropIndex("dbo.ProductOrders", new[] { "Product_Id" });
            DropIndex("dbo.ProductProductBundles", new[] { "ProductBundle_Id" });
            DropIndex("dbo.ProductProductBundles", new[] { "Product_Id" });
            DropIndex("dbo.ProductBundleOrders", new[] { "Order_Id" });
            DropIndex("dbo.ProductBundleOrders", new[] { "ProductBundle_Id" });
            DropIndex("dbo.Orders", new[] { "Customer_Id" });
            CreateIndex("dbo.ProductOrders", "Order_id");
            CreateIndex("dbo.ProductOrders", "Product_id");
            CreateIndex("dbo.ProductProductBundles", "ProductBundle_id");
            CreateIndex("dbo.ProductProductBundles", "Product_id");
            CreateIndex("dbo.ProductBundleOrders", "Order_id");
            CreateIndex("dbo.ProductBundleOrders", "ProductBundle_id");
            CreateIndex("dbo.Orders", "customer_id");
        }
    }
}
