namespace CoolBlue.EF.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Addedusernameandpasswordforemployeelogin : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Employees", "username", c => c.String());
            AddColumn("dbo.Employees", "password", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Employees", "password");
            DropColumn("dbo.Employees", "username");
        }
    }
}
