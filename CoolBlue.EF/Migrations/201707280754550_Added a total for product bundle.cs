namespace CoolBlue.EF.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Addedatotalforproductbundle : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.ProductBundles", "price", c => c.Double(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.ProductBundles", "price");
        }
    }
}
