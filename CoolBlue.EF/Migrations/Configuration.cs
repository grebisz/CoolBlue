namespace CoolBlue.EF.Migrations
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<CoolBlue.EF.Context>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(CoolBlue.EF.Context context)
        {
            context.Employees.AddOrUpdate(p => p.Firstname, 
                new Employee() { Firstname = "Gregory", Lastname = "Rebisz", Active = true, Username = "grebisz", Password = "panopticon" },
                new Employee() { Firstname = "Inactive", Lastname = "User", Active = false, Username = "helios", Password = "heliospass"}
            );
            context.Products.AddOrUpdate(p => p.Title,
                new Product() { Title = "Lenovo IdeaPad 510-15IKB 80SV00K5MH", Description = "Lenovo Laptop", Price = 699 },
                new Product() { Title = "HP 15-ba071nd", Description = "Hewlett Packard Laptop", Price = 369 },
                new Product() { Title = "MSI GP72M 7REX-687NL Leopard Pro", Description = "MSI Leopard Pro", Price = 1549 },
                new Product() { Title = "LG 43UH610V", Description = "LG 4K UHD TV", Price = 412 },
                new Product() { Title = "Samsung UE43KU6000", Description = "Samsung 4k UHD TV", Price = 604 },
                new Product() { Title = "VOGEL'S THIN 205", Description = "Display Mounting Rack", Price = 59 },
                new Product() { Title = "VOGEL'S 305", Description = "Display Heavy-Duty Mounting Rack", Price = 89 }
            );

            context.Customers.AddOrUpdate(p => p.Firstname,
                new Customer() { Firstname = "Joe", Lastname = "Soap", Email = "JoeSoap@gmail.com" }
            );

            context.SaveChanges();
            //Product Bundles Addition
            Product vogel = context.Products.First(p => p.Title == "VOGEL'S THIN 205");
            Product Samsung = context.Products.First(p => p.Title == "Samsung UE43KU6000");
            Product LG = context.Products.First(p => p.Title == "LG 43UH610V");

            ProductBundle bundle1 = new ProductBundle()
            {
                Title = "Samsung UHD TV + Mount",
                Description = "Mounted UHD TV setup",
                Price = vogel.Price + Samsung.Price,
                Products = new System.Collections.Generic.List<Product>()
            };
            bundle1.Products.Add(vogel);
            bundle1.Products.Add(Samsung);
            
            ProductBundle bundle2 = new ProductBundle()
            {
                Title = "LG UHD TV + Mount",
                Description = "Mounted UHD TV setup",
                Price = vogel.Price + LG.Price,
                Products = new System.Collections.Generic.List<Product>()
            };
            bundle2.Products.Add(vogel);
            bundle2.Products.Add(LG);

            context.Productbundles.AddOrUpdate(p => p.Title,
                bundle1, bundle2
            );
        }
    }
}
