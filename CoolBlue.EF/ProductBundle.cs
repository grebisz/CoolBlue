﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CoolBlue.EF
{
    public class ProductBundle
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public List<Product> Products { get; set; } 
        public List<Order> Orders { get; set; }
        public double Price { get; set; }
    }
}
