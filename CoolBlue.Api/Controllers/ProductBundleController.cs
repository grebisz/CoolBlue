﻿using CoolBlue.Core.Concrete;
using CoolBlue.Core.EfRepository;
using CoolBlue.EF;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace CoolBlue.Api.Controllers
{
    public class ProductBundleController : ApiController
    {
        static Repository repo = new Repository(Core.Enums.RepositoryFactoryTypes.Mock);
        // GET api/Products
        public List<ProductBundle> Get()
        {
            return repo.ProductBundles.GetAll();
        }

        // GET api/values/5
        
        public ProductBundle Get(int id)
        {
            return repo.ProductBundles.GetById(id);
        }

        // POST api/values
        public void Post([FromBody]string value)
        {
        }

        // PUT api/values/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/values/5
        public void Delete(int id)
        {
        }
    }
}
