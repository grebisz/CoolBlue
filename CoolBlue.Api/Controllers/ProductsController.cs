﻿using CoolBlue.Core.Concrete;
using CoolBlue.EF;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace CoolBlue.Api.Controllers
{
    public class ProductsController : ApiController
    {
        static Repository repo = new Repository(Core.Enums.RepositoryFactoryTypes.Mock);
        // GET api/Products
        public IEnumerable<Product> Get()
        {
            return repo.Products.GetAll();
        }

        // GET api/values/5
        public Product Get(int id)
        {
            return repo.Products.GetById(id);
        }

        //NB: Should be an integer and fetch by ID
        [Route("api/Products/{name}/bundles")]
        public List<ProductBundle> Get(string name)
        {
            return repo.ProductBundles.GetRelatedBundles(name);
        }

        // POST api/values
        public void Post([FromBody]string value)
        {
        }

        // PUT api/values/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/values/5
        public void Delete(int id)
        {
        }
    }
}
